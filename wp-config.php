<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'voiceworks_intouch' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',_Smf2hxg[U!ZiA00nF$.4lr0xE^n|(eVYo/t6lPz[_bmc^}n@z`NVwOW.Sx4bp5' );
define( 'SECURE_AUTH_KEY',  '~~P.pR(CJqiOu|!.Op}YO?(t,h^*V?Ya7,r*K5p!Xw8lNV#zhbhAh$DJ<qiEiO!V' );
define( 'LOGGED_IN_KEY',    'Zx^2UObGfySK`^peH|9E$wb<no^{+.a_y$!?yHOBu|1KVStk<=tbSj9E$=<,[*bc' );
define( 'NONCE_KEY',        '|ig5$zL0/1|w}`HACJ9kUD`;Qn8;V0^t{5L1LxbR%(!? BR7JsM$ypN<UG4^)^B ' );
define( 'AUTH_SALT',        '`7]>Wo6WftrkWQ28-*|@I0W#p?Yplz>T[d0+Rfi&-pl]OI%,*e>6V4`>~fk.{385' );
define( 'SECURE_AUTH_SALT', 'vt 47DsT!?pzNlY=*>-84JisD:Glsw_J7j^IKJaU1|RF>teW[U0~,DEzX84{W^o%' );
define( 'LOGGED_IN_SALT',   'oT%>MQ8%gCQ=#998P:oA/]Lh3K*<.6T|/.XI[eI~OG%~!aVwqTMDb?pzNjh!5ugG' );
define( 'NONCE_SALT',       '-jwntaf~^rzs]h,_&8VotcCIFem8`6G;VM1@I1h1%Rm_mvo2Q6Eo?Rp*8&^l@YX0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'vw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

@ini_set('upload_max_size' , '256M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/** Includes the file.php before calling wp_handle_upload */
require_once( ABSPATH . 'wp-admin/includes/file.php' );
