��            )   �      �  
   �  	   �     �  
   �  	   �      �  $   �          '     5  )   F     p     u     {  W   �  a   �     :  	   ?     I  %   W     }  F   �  \   �     1     8     M     \     k     t  �  �               ,     :     G      T  $   u  
   �     �     �      �  	   �     �     �  Z      d   [     �     �     �  3   �     	  H   1	  �   z	     
     	
     
     *
     8
     A
                                                                                
                                               	          % Comments %1$s Edit %1$s Previous 0 Comments 1 Comment <span uk-pagination-next></span> <span uk-pagination-previous></span> Comment Comments (%s) Continue reading Display your sites breadcrumb navigation. Edit Email Home It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Name Next %1$s Nothing Found Oops! That page can&rsquo;t be found. Posted in %1$s. Ready to publish your first post? <a href="%1$s">Get started here</a>. Sorry, but nothing matched your search terms. Please try again with some different keywords. Title: Written by %s on %s. Written by %s. Written on %s. YOOtheme YOOtheme Builder Project-Id-Version: VERSION
POT-Creation-Date: 2019-04-18 11:35+0000
PO-Revision-Date: 2019-04-18 11:35+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE TEAM <EMAIL@ADDRESS>
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
 % Comentarii %1$s Editează %1$s Anterior 0 Comentarii 1 Comentariu <span uk-pagination-next></span> <span uk-pagination-previous></span> Comentariu Comentarii (%s) Continuă lectura Afișează un traseu de navigare Editează E-mail Acasă Pare că &rsquo;nu putem găsi ceea ce &rsquo;tu cauți. Poate că e de ajutor o căutare. Conectat ca <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Deconectare?</a> Nume Următor %1$s N-a fost găsit nimic. Oops! Această pagină &rsquo;nu poate fi găsită. Publicat în %1$s. Pregătit să publici prima ta postare? <a href="%1$s">Începe aici</a>. Ne pare rău, dar nimic nu corespunde cu termenii dvs de căutare. Vă rugăm să încercați din nou cu niște cuvinte cheie diferite. Titlu: Scris de %s în %s. Scris de %s. Scris în %s. YOOtheme YOOtheme Builder 