<?php

use YOOtheme\Builder\Wordpress\BuilderListener;
use YOOtheme\Builder\Wordpress\ContentListener;

$config = [

    'name' => 'yootheme/builder-wordpress',

    'main' => function ($app) {

        $app->subscribe(new ContentListener());
        $app->subscribe(new BuilderListener($this->path));

    },

    'routes' => function ($routes) {

        $routes->post('/builder/image', 'YOOtheme\Builder\Wordpress\BuilderController:loadImage');

    },

];

return defined('WPINC') ? $config : false;
