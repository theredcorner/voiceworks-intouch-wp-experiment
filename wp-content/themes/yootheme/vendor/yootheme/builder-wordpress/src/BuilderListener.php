<?php

namespace YOOtheme\Builder\Wordpress;

use YOOtheme\EventSubscriber;

class BuilderListener extends EventSubscriber
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var array
     */
    public $inject = [
        'view' => 'app.view',
        'builder' => 'app.builder',
        'sections' => 'app.view.sections',
    ];

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function onInit($builder)
    {
        $builder->addTypePath("{$this->path}/elements/*/element.json");

        if (is_child_theme()) {
            $builder->addTypePath(get_stylesheet_directory() . '/builder/*/element.json');
        }
    }

    public function onSite()
    {
        $this->view->addLoader(function ($name, $parameters, $next) {
            return do_shortcode($next($name, $parameters));
        }, '*/builder/elements/layout/templates/template.php');
    }

    public function onContentPrepare($obj)
    {
        if (!$this->sections->exists('builder') && isset($obj->content)) {
            $this->sections->set('builder', function () use ($obj) {
                $result = $this->builder->render($obj->content, ['prefix' => 'page']);
                $this->app->trigger('content', [$result]);
                return $result;
            });
        }

        $this->theme->set('builder', $this->sections->exists('builder'));
    }

    public static function getSubscribedEvents()
    {
        return [
            'builder.init' => 'onInit',
            'theme.site' => 'onSite',
            'content.prepare' => 'onContentPrepare',
        ];
    }
}
