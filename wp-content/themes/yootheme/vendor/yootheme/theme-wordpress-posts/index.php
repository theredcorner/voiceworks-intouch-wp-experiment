<?php

use YOOtheme\Theme\Wordpress\PostsListener;

$config = [

    'name' => 'yootheme/wordpress-posts',

    'main' => function ($app) {

        $app->subscribe(new PostsListener());

    },

];

return defined('WPINC') ? $config : false;
