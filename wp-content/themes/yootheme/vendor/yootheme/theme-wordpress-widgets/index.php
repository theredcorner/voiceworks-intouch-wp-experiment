<?php

use YOOtheme\Theme\WidgetsListener;

$config = [

    'name' => 'yootheme/wordpress-widgets',

    'main' => function ($app) {

        $app->subscribe($app['wordpress-widgets'] = new WidgetsListener($this->path));

    },

];

return defined('WPINC') ? $config : false;
