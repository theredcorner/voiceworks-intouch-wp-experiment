<?php

namespace YOOtheme\Theme;

use YOOtheme\EventSubscriber;
use YOOtheme\Util\Collection;

class WidgetsListener extends EventSubscriber
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $style;

    /**
     * @var string
     */
    public $sidebar;

    /**
     * @var array
     */
    public $widgets = [];

    /**
     * @var array
     */
    public $position = [];

    /**
     * @var array
     */
    public $inject = [
        'view' => 'app.view',
        'config' => 'app.config',
        'scripts' => 'app.scripts',
    ];

    /**
     * Constructor.
     *
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    public function onStart()
    {
        add_action('widgets_init', function () {
            register_widget('YOOtheme\Theme\BreadcrumbsWidget');
            require "{$this->path}/src/BuilderWidget.php";
            register_widget('BuilderWidget');
        });
    }

    public function onInit()
    {
        add_action('current_screen', [$this, 'editScreen']);
        add_action('in_widget_form', [$this, 'editWidget'], 10, 3);
        add_filter('widget_update_callback', [$this, 'updateWidget'], 10, 3);
    }

    public function onSite()
    {
        add_action('is_active_sidebar', [$this, 'isActiveSidebar'], null, 2);
        add_action('dynamic_sidebar_before', [$this, 'beforeSidebar']);
        add_action('dynamic_sidebar_after', [$this, 'afterSidebar']);
        add_filter('sanitize_title', [$this, 'parseSidebarStyle'], 10, 2);
        add_filter('widget_display_callback', [$this, 'displayWidget'], 10, 3);
    }

    public function isActiveSidebar($active, $sidebar)
    {
        return $active
            || has_nav_menu($sidebar)
            || in_array($sidebar, [$this->theme->get('header.search'), $this->theme->get('header.social')]);
    }

    public function beforeSidebar($sidebar)
    {
        $this->sidebar = $sidebar;
        $this->widgets[$sidebar] = [];
    }

    public function afterSidebar($sidebar)
    {
        global $wp_widget_factory, $wp_registered_sidebars;

        $search = $this->theme->get('header.search');

        if ($sidebar == $search || $search && $sidebar == 'mobile') {
            $this->displayWidget([], $wp_widget_factory->widgets['WP_Widget_Search'], $wp_registered_sidebars[$sidebar]);
        }

        $items = $this->widgets[$sidebar];

        if ($sidebar == $this->theme->get('header.social')) {

            $widget = $this->createWidget([
                'id' => 'social',
                'type' => 'social',
                'content' => $this->view->render('socials'),
            ]);

            strpos($sidebar, 'left') ? array_unshift($items, $widget) : array_push($items, $widget);
        }

        $location = $sidebar == 'navbar-split' ? 'navbar' : $sidebar;

        if (has_nav_menu($location)) {

            $menu = get_nav_menu_locations();

            ob_start();
            wp_nav_menu([
                'theme_location' => $location,
                'menu' => $menu[$location],
                'split' => $location == 'navbar',
            ]);

            $widget = $this->createWidget([
                'id' => "menu-{$sidebar}",
                'type' => 'menu',
                'content' => ob_get_clean(),
            ]);

            array_unshift($items, $widget);
        }

        echo $this->view->render('position', [
            'name' => $sidebar,
            'items' => $items,
            'style' => $this->style,
            'position' => $this->position
        ]);

        $this->style = null;
        $this->sidebar = null;
        $this->position = null;
    }

    public function parseSidebarStyle($title, $raw)
    {
        global $wp_registered_sidebars;

        if (strpos($raw, ':')) {

            list($name, $style) = explode(':', $raw, 2);

            if (isset($wp_registered_sidebars[$name])) {
                $this->style = $style;
                return $name;
            }
        }

        return $title;
    }

    public function displayWidget($instance, $widget, $args)
    {
        if ($instance === false) {
            return false;
        }

        // disable wpautop filter for text-widget in navbar, header and toolbar position
        // re-add it afterwards for subsequent text-widgets
        if (in_array($this->sidebar, ['navbar', 'header', 'toolbar-left', 'toolbar-right'])) {
            $priority = has_filter('widget_text_content', 'wpautop');
            if ($priority !== false) {
                remove_filter('widget_text_content', 'wpautop', $priority);
                add_filter('widget_text_content', [$this, '_restore_textwidget_wpautop'], $priority + 1);
            }
        }

        ob_start();
        $widget->widget($args, $instance);
        $output = ob_get_clean();

        preg_match('/' . preg_quote($args['before_widget'], '/') . '(.*?)' . preg_quote($args['after_widget'], '/') . '/s', $output, $content);
        preg_match('/' . preg_quote($args['before_title'], '/') . '(.*?)' . preg_quote($args['after_title'], '/') . '/s', $output, $title);

        $type = strtr(str_replace('nav_menu', 'menu', $widget->id_base), '_', '-');
        $config = json_decode(isset($instance[$key = '_theme']) ? $instance[$key] : '{}', true);
        $content = $content ? $content[1] : $output;

        if ($title) {
            $content = str_replace($title[0], '', $content);
        }

        // add 'uk-panel' to text widget content div class
        if ($type === 'text') {
            $content = substr_replace($content, 'uk-panel ', strpos($content, 'class="textwidget"') + strlen('class="'), 0);
        }

        if (!isset($widget->widget_cssclass)) {
            $widget->widget_cssclass = '';
        }

        $config['is_list'] = $this->isList($type);

        $this->widgets[$this->sidebar][] = $this->createWidget([
            'id' => $widget->id,
            'type' => $type,
            'title' => $title ? $title[1] : '',
            'content' => $content,
            'attrs' => ['id' => "widget-{$widget->id}", 'class' => [trim("widget-{$type} {$widget->widget_cssclass}")]],
            'config' => new Collection($config),
        ]);

        return false;
    }

    public function _restore_textwidget_wpautop($content)
    {
        $current_priority = has_filter('widget_text_content', [$this, '_restore_textwidget_wpautop']);

        add_filter('widget_text_content', 'wpautop', $current_priority - 1);
        remove_filter('widget_text_content', [$this, '_restore_textwidget_wpautop'], $current_priority);

        return $content;
    }

    public function isList($type)
    {
        return in_array($type, ['recent-posts', 'pages', 'recent-comments', 'archives', 'categories', 'meta']);
    }

    public function editScreen($screen)
    {
        if (in_array($screen->base, ['customize', 'widgets'])) {

            $config = $this->config->load("{$this->path}/config/widgets.json");

            $this->scripts->add('widgets-data', sprintf('var $widgets = %s;', json_encode($config)), [], 'string');

            if ($screen->base === 'widgets') {
                $this->scripts->add('widgets', "{$this->path}/app/widgets.min.js", ['uikit', 'widgets-data']);
            }
        }
    }

    public function editWidget($widget, $return, $instance)
    {
        $config = isset($instance[$key = '_theme']) ? esc_attr($instance[$key]) : '{}';

        echo "<input type=\"hidden\" name=\"{$widget->get_field_name($key)}\" value=\"{$config}\" data-widget>";
    }

    public function updateWidget($instance, $new_instance)
    {
        if (isset($new_instance['_theme'])) {
            $instance['_theme'] = $new_instance['_theme'];
        }

        return $instance;
    }

    public function createWidget($widget)
    {
        static $id = 0;

        return (object) array_merge([
            'id' => 'tm-' . (++$id),
            'title' => '',
            'position' => $this->sidebar,
            'attrs' => ['class' => []],
            'config' => new Collection(),
        ], (array) $widget);
    }

    public static function getSubscribedEvents()
    {
        return [
            'init' => 'onStart',
            'theme.init' => 'onInit',
            'theme.site' => 'onSite',
        ];
    }
}
