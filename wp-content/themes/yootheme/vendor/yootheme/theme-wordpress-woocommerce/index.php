<?php

use YOOtheme\Theme\Wordpress\WooCommerceListener;

$config = [

    'name' => 'yootheme/wordpress-woocommerce',

    'main' => function ($app) {

        $app->subscribe(new WooCommerceListener());

    },

];

return defined('WPINC') ? $config : false;
