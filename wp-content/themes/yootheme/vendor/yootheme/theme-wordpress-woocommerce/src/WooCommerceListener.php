<?php

namespace YOOtheme\Theme\Wordpress;

use YOOtheme\EventSubscriber;
use YOOtheme\Util\File;

class WooCommerceListener extends EventSubscriber
{
    /**
     * @var array
     */
    public $inject = [
        'styler' => 'app.styler',
    ];

    public function onInit($theme)
    {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';

        // ignore files from being compiled into theme.css
        if (!is_plugin_active('woocommerce/woocommerce.php')) {
            $this->styler->config->merge(['ignore_less' => ['woocommerce.less']]);
        }
    }

    public function onSite($theme)
    {
        // remove woocommerce general style
        add_filter('woocommerce_enqueue_styles', function ($styles) {
            unset($styles['woocommerce-general']);
            return $styles;
        });

        // with woocommerce 3.3.x, setting is available in the WP customizer
        add_filter('loop_shop_per_page', function ($items) {
            return $this->theme->get('woocommerce.items') ?: $items;
        }, 20);
    }

    public function onAdmin($theme)
    {
        // check if theme css needs to be updated
        $style = new File("@theme/css/theme{.{$theme->id},}.css");
        $compiled = strpos($style->getContents(), '.woocommerce');

        if (is_plugin_active('woocommerce/woocommerce.php') xor $compiled) {
            $this->styler->config->set('section.update', true);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'theme.init' => 'onInit',
            'theme.site' => 'onSite',
            'theme.admin' => 'onAdmin',
        ];
    }
}
