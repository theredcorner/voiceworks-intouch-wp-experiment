<?php

use YOOtheme\Theme\Wordpress\ChildThemeListener;
use YOOtheme\Theme\Wordpress\CustomizerListener;
use YOOtheme\Theme\Wordpress\SystemCheck;
use YOOtheme\Theme\Wordpress\ThemeListener;
use YOOtheme\Theme\Wordpress\UpgradeListener;
use YOOtheme\Theme\Wordpress\UrlListener;

$config = [

    'name' => 'yootheme/wordpress-theme',

    'main' => function ($app) {

        $app['trans'] = $app->protect(function ($id) {
            return __($id, 'yootheme');
        });

        $app['apikey'] = function () {
            return $this->get('yootheme_apikey');
        };

        $app['systemcheck'] = function () {
            return new SystemCheck();
        };

        $app->locator
            ->addPath("{$this->path}/app", 'app')
            ->addPath("{$this->path}/assets", 'assets');

        $app->subscribe(new ThemeListener($this->path, $this->config))
            ->subscribe(new ChildThemeListener())
            ->subscribe(new CustomizerListener())
            ->subscribe(new UrlListener())
            ->subscribe(new UpgradeListener());
    },

    'config' => [

        'defaults' => [

            'post' => [
                'width' => '',
                'padding' => '',
                'content_width' => '',
                'image_align' => 'top',
                'image_margin' => 'medium',
                'image_width' => '',
                'image_height' => '',
                'header_align' => 0,
                'title_margin' => 'default',
                'meta_align' => 'bottom',
                'meta_margin' => 'default',
                'meta_style' => 'sentence',
                'content_margin' => 'medium',
                'content_dropcap' => 0,
                'navigation' => 1,
                'date' => 1,
                'author' => 1,
                'categories' => 1,
                'tags' => 1,
            ],

            'blog' => [
                'width' => '',
                'padding' => '',
                'column' => 1,
                'grid_column_gap' => '',
                'grid_row_gap' => '',
                'grid_breakpoint' => 'm',
                'image_align' => 'top',
                'image_margin' => 'medium',
                'image_width' => '',
                'image_height' => '',
                'header_align' => 0,
                'title_style' => '',
                'title_margin' => 'default',
                'meta_margin' => 'default',
                'content_excerpt' => 0,
                'content_length' => '',
                'content_margin' => 'medium',
                'content_align' => 0,
                'button_style' => 'default',
                'button_margin' => 'medium',
                'navigation' => 'pagination',
                'date' => 1,
                'author' => 1,
                'categories' => 1,
                'comments' => 1,
                'content' => 1,
                'tags' => 0,
                'button' => 1,
                'category_title' => 0,
            ],

            'minimum_stability' => 'stable',

        ],

    ],

];

return defined('WPINC') ? $config : false;
