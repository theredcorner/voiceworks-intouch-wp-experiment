<?php

namespace YOOtheme\Theme\Wordpress;

use YOOtheme\EventSubscriber;
use YOOtheme\Util\Str;

class ThemeListener extends EventSubscriber
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var Collection
     */
    public $config;

    /**
     * @var array
     */
    public $inject = [
        'url' => 'app.url',
        'view' => 'app.view',
        'sections' => 'app.view.sections',
    ];

    /**
     * Constructor.
     *
     * @param string     $path
     * @param Collection $config
     */
    public function __construct($path, $config)
    {
        $this->path = $path;
        $this->config = $config;
    }

    public function onStart($app)
    {
        $app['config']->add('app', [
            'platform' => 'wordpress',
        ]);

        $app['config']->addFile('theme', "{$this->path}/config/theme.json", ['theme' => $this->theme]);

        $app['kernel']->addMiddleware(function ($request, $response, $next) {

            // check user capabilities
            if (!$request->getAttribute('allowed') && !current_user_can('edit_theme_options')) {
                $this->app->abort(403, 'Insufficient User Rights.');
            }

            return $next($request, $response);
        });

        add_action('wp_loaded', function () {
            $this->app->trigger('theme.init', [$this->theme]);
        });
    }

    public function onInit($theme)
    {
        $theme['updater']->add("{$this->path}/updates.php");

        $config = get_theme_mod('config', '{}');
        $config = json_decode($config, true);
        $config = $theme['updater']->update($config, ['theme' => $theme, 'app' => $this->app]);

        // set defaults
        $this->theme->merge($this->config['defaults'], true);
        $this->theme->merge($config, true);

        $this->url->addResolver(function ($path, $parameters, $secure, $next) {

            $uri = $next($path, $parameters, $secure, $next);

            if (Str::startsWith($uri->getQueryParam('p'), 'theme/') && $this->app['config']->get('theme.customizer')) {

                $query = $uri->getQueryParams();
                $query['wp_customize'] = 'on';

                $uri = $uri->withQueryParams($query);
            }

            return $uri;
        });

        if (!$this->app['admin']) {
            $this->app->trigger('theme.site', [$this->theme]);
        } elseif ($this->app['config']->get('theme.customizer')) {
            $this->app->trigger('theme.admin', [$this->theme]);
        }

        add_filter('upload_mimes', function ($mimes) {

            $mimes['svg|svgz'] = 'image/svg+xml';

            return $mimes;
        });

        add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {

            if (empty($data['type']) && substr($filename, -4) === '.svg') {
                $data['ext'] = 'svg';
                $data['type'] = 'image/svg+xml';
            }

            return $data;

        }, 10, 4);

    }

    public function onSite($theme)
    {
        $theme
            ->set('direction', is_rtl() ? 'rtl' : 'lrt')
            ->set('site_url', rtrim(get_bloginfo('url'), '/'))
            ->set('page_class', ''); // TODO: implement page class builder

        if ($theme->get('disable_wpautop')) {
            remove_filter('the_content', 'wpautop');
            remove_filter('the_excerpt', 'wpautop');
        }

        if ($theme->get('disable_emojis')) {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_action('admin_print_styles', 'print_emoji_styles');
            remove_filter('the_content_feed', 'wp_staticize_emoji');
            remove_filter('comment_text_rss', 'wp_staticize_emoji');
            remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        }

        $this->sections->add('breadcrumbs', function () {
            return $this->view->render('breadcrumbs', ['items' => Breadcrumbs::getItems()]);
        });
    }

    public function onSiteHead($theme)
    {
        $custom = $theme->get('custom_js') ?: '';

        if ($theme->get('jquery') || strpos($custom, 'jQuery') !== false) {
            wp_enqueue_script('jquery');
        }

        add_action('wp_head', function () use ($custom) {

            if ($custom) {

                if (stripos(trim($custom), '<script') === 0) {
                    echo $custom;
                } else {
                    echo "<script>try { {$custom}\n } catch (e) { console.error('Custom Theme JS Code: ', e); }</script>";
                }
            }

        }, 20);
    }

    public static function getSubscribedEvents()
    {
        return [
            'init' => 'onStart',
            'theme.init' => ['onInit', -5],
            'theme.site' => [['onSite', 10], ['onSiteHead', 0]],
        ];
    }
}
