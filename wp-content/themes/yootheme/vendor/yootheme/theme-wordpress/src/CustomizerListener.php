<?php

namespace YOOtheme\Theme\Wordpress;

use YOOtheme\EventSubscriber;

class CustomizerListener extends EventSubscriber
{
    public $active = false;

    public $inject = [
        'config' => 'app.config',
        'styles' => 'app.styles',
        'scripts' => 'app.scripts',
    ];

    public function onInit()
    {
        add_action('customize_register', function ($customizer) {

            // add settings
            $customizer->add_setting('config');
            $customizer->add_setting('page');
            $customizer->remove_setting('site_icon');

            // encode config
            add_filter('customize_sanitize_js_config', function ($value) {
                return base64_encode($this->theme->config->json());
            });

            // decode config
            add_filter('customize_sanitize_config', function ($value) {
                return base64_decode($value);
            });

            // decode page
            add_filter('customize_sanitize_page', function ($value) {
                return json_decode(base64_decode($value), true);
            });

            // remove page
            add_action('customize_save', function ($customizer) {
                $customizer->remove_setting('page');
            });

            // set customizer active
            $this->config->add('theme', ['customizer' => true]);

        }, 10);
    }

    public function onAdmin()
    {
        // add assets
        $this->styles->add('customizer', '@assets/css/admin.css');
        $this->scripts->add('customizer', '@app/customizer.min.js', ['uikit', 'config']);
    }

    public function onView()
    {
        // add data
        if ($this->config->get('theme.customizer') && $data = $this->config->get('customizer')) {
            $this->scripts->add('customizer-data', sprintf('var $customizer = %s;', json_encode($data)), 'customizer', 'string');
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'init' => 'onInit',
            'view' => 'onView',
            'theme.admin' => 'onAdmin',
        ];
    }
}
