<?php

namespace YOOtheme\Theme\Wordpress;

use YOOtheme\EventSubscriber;

class EditorListener extends EventSubscriber
{
    public function onAdmin()
    {
        // init on certain admin screens
        add_action('current_screen', function ($screen) {

            if ($screen->base != 'customize') {
                return;
            }

            add_action('admin_print_footer_scripts', function () {

                if (function_exists('wp_enqueue_editor')) {

                    // WordPress 4.8 and later
                    wp_enqueue_editor();

                } else {

                    // WordPress 4.7 and earlier. Can be removed when not needed anymore.
                    // If removed: Will fail gracefully and fall back to code editor only

                    wp_enqueue_script('utils');
                    wp_enqueue_script('wplink');

                    // create dummy editor to initialize tinyMCE
                    echo '<div style="display:none">';
                    wp_editor('', 'yo-editor-dummy', [
                        'wpautop' => false,
                        'media_buttons' => true,
                        'textarea_name' => 'textarea-dummy-yootheme',
                        'textarea_rows' => 10,
                        'editor_class' => 'horizontal',
                        'teeny' => false,
                        'dfw' => false,
                        'tinymce' => true,
                        'quicktags' => true,
                    ]);
                    echo '</div>';
                }

            });

        });
    }

    public static function getSubscribedEvents()
    {
        return [
            'theme.admin' => 'onAdmin',
        ];
    }
}
