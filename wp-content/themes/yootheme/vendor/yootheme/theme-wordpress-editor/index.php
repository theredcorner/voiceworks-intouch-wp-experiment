<?php

use YOOtheme\Theme\Wordpress\EditorListener;

$config = [

    'name' => 'yootheme/wordpress-editor',

    'main' => function ($app) {

        $app->subscribe(new EditorListener());

    },

];

return defined('WPINC') ? $config : false;
