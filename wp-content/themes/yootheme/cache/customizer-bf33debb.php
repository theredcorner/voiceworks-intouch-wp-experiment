<?php // @file C:/wamp64/www/theredcorner/voiceworks-intouch/wp-content/themes/yootheme/vendor/yootheme/theme/../theme-wordpress/config/customizer.json

return [
  'panels' => [
    'advanced' => [
      'fields' => [
        'disable_wpautop' => [
          'label' => 'Filter', 
          'description' => 'Disable the <a href="https://developer.wordpress.org/reference/functions/wpautop/" target="_blank">wpautop</a> filter for the_content and the_excerpt.', 
          'type' => 'checkbox', 
          'text' => 'Disable wpautop'
        ], 
        'disable_emojis' => [
          'description' => 'Disable conversion of Emoji characters to images.', 
          'type' => 'checkbox', 
          'text' => 'Disable Emojis'
        ]
      ]
    ], 
    'api-key' => [
      'fields' => [
        'minimum_stability' => [
          'label' => 'Minimum Stability', 
          'description' => 'The minimum stability of the theme updates. Stable is recommended for production websites, Beta is only for testing new features and reporting issues.', 
          'type' => 'select', 
          'options' => [
            'Stable' => 'stable', 
            'Beta' => 'beta'
          ]
        ]
      ]
    ], 
    'system-post' => [
      'title' => 'Post', 
      'width' => 400, 
      'fields' => [
        'post.width' => [
          'label' => 'Width', 
          'description' => 'Set the post width. The image and content can\'t expand beyond this width.', 
          'type' => 'select', 
          'options' => [
            'XSmall' => 'xsmall', 
            'Small' => 'small', 
            'Default' => '', 
            'Large' => 'large', 
            'Expand' => 'expand', 
            'None' => 'none'
          ]
        ], 
        'post.padding' => [
          'label' => 'Padding', 
          'description' => 'Set the vertical padding.', 
          'type' => 'select', 
          'options' => [
            'Default' => '', 
            'XSmall' => 'xsmall', 
            'Small' => 'small', 
            'Large' => 'large', 
            'XLarge' => 'xlarge'
          ]
        ], 
        'post.padding_remove' => [
          'type' => 'checkbox', 
          'text' => 'Remove top padding'
        ], 
        'post.content_width' => [
          'label' => 'Content Width', 
          'description' => 'Set an optional content width which doesn\'t affect the image.', 
          'type' => 'select', 
          'options' => [
            'Auto' => '', 
            'XSmall' => 'xsmall', 
            'Small' => 'small'
          ], 
          'enable' => 'post.width != \'xsmall\''
        ], 
        'post.image_align' => [
          'label' => 'Image Alignment', 
          'description' => 'Align the image to the top or place it between the title and the content.', 
          'type' => 'select', 
          'options' => [
            'Top' => 'top', 
            'Between' => 'between'
          ]
        ], 
        'post.image_margin' => [
          'label' => 'Image Margin', 
          'description' => 'Set the top margin if the image is aligned between the title and the content.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ], 
          'enable' => 'post.image_align == \'between\''
        ], 
        'post.image_dimension' => [
          'type' => 'grid', 
          'description' => 'Setting just one value preserves the original proportions. The image will be resized and cropped automatically and where possible, high resolution images will be auto-generated.', 
          'fields' => [
            'post.image_width' => [
              'label' => 'Image Width', 
              'width' => '1-2', 
              'attrs' => [
                'placeholder' => 'auto', 
                'lazy' => true
              ]
            ], 
            'post.image_height' => [
              'label' => 'Image Height', 
              'width' => '1-2', 
              'attrs' => [
                'placeholder' => 'auto', 
                'lazy' => true
              ]
            ]
          ]
        ], 
        'post.header_align' => [
          'label' => 'Alignment', 
          'description' => 'Align the title and meta text.', 
          'type' => 'checkbox', 
          'text' => 'Center the title and meta text'
        ], 
        'post.title_margin' => [
          'label' => 'Title Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'post.meta_align' => [
          'label' => 'Meta Alignment', 
          'description' => 'Position the meta text above or below the title.', 
          'type' => 'select', 
          'options' => [
            'Top' => 'top', 
            'Bottom' => 'bottom'
          ]
        ], 
        'post.meta_margin' => [
          'label' => 'Meta Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'post.meta_style' => [
          'label' => 'Meta Style', 
          'description' => 'Display the meta text in a sentence or a horizontal list.', 
          'type' => 'select', 
          'options' => [
            'List' => 'list', 
            'Sentence' => 'sentence'
          ]
        ], 
        'post.content_margin' => [
          'label' => 'Content Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'post.content_dropcap' => [
          'label' => 'Drop Cap', 
          'description' => 'Set a large initial letter that drops below the first line of the first paragraph.', 
          'type' => 'checkbox', 
          'text' => 'Show drop cap'
        ], 
        'post.navigation' => [
          'label' => 'Navigation', 
          'description' => 'Enable a navigation to move to the previous or next post.', 
          'type' => 'checkbox', 
          'text' => 'Show navigation'
        ], 
        'post.date' => [
          'label' => 'Display', 
          'type' => 'checkbox', 
          'text' => 'Show date'
        ], 
        'post.author' => [
          'type' => 'checkbox', 
          'text' => 'Show author'
        ], 
        'post.categories' => [
          'type' => 'checkbox', 
          'text' => 'Show categories'
        ], 
        'post.tags' => [
          'description' => 'Show system fields for single posts. This option does not apply to the blog.', 
          'type' => 'checkbox', 
          'text' => 'Show tags'
        ]
      ], 
      'help' => [
        'Post' => [[
            'title' => 'Layout', 
            'src' => 'site/docs/yootheme-pro/post/wordpress/videos/layout.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/post#layout', 
            'support' => 'support/search?tags=125&q=post'
          ], [
            'title' => 'Image', 
            'src' => 'site/docs/yootheme-pro/post/wordpress/videos/image.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/post#image', 
            'support' => 'support/search?tags=125&q=post'
          ], [
            'title' => 'Content', 
            'src' => 'site/docs/yootheme-pro/post/wordpress/videos/content.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/post#content', 
            'support' => 'support/search?tags=125&q=post'
          ], [
            'title' => 'Navigation', 
            'src' => 'site/docs/yootheme-pro/post/wordpress/videos/navigation.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/post#navigation', 
            'support' => 'support/search?tags=125&q=post'
          ]], 
        'Post Builder' => [[
            'title' => 'Post Builder', 
            'src' => 'site/docs/yootheme-pro/post-builder/wordpress/videos/post-builder.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/post-builder', 
            'support' => 'support/search?tags=125&q=builder'
          ]], 
        'Image Field' => [[
            'title' => 'Images', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/images.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#images', 
            'support' => 'support/search?tags=125&q=image%20field'
          ], [
            'title' => 'Media Manager', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/media-manager.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#media-manager', 
            'support' => 'support/search?tags=125&q=media%20manager'
          ], [
            'title' => 'Unsplash Library', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/unsplash.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#unsplash-library', 
            'support' => 'support/search?tags=125&q=unsplash'
          ]]
      ]
    ], 
    'system-blog' => [
      'title' => 'Blog', 
      'width' => 400, 
      'fields' => [
        'blog.width' => [
          'label' => 'Width', 
          'description' => 'Set the blog width.', 
          'type' => 'select', 
          'options' => [
            'Default' => '', 
            'Small' => 'small', 
            'Large' => 'large', 
            'Expand' => 'expand'
          ]
        ], 
        'blog.padding' => [
          'label' => 'Padding', 
          'description' => 'Set the vertical padding.', 
          'type' => 'select', 
          'options' => [
            'Default' => '', 
            'XSmall' => 'xsmall', 
            'Small' => 'small', 
            'Large' => 'large', 
            'XLarge' => 'xlarge'
          ]
        ], 
        'blog.column' => [
          'label' => 'Columns', 
          'type' => 'select', 
          'options' => [
            1 => 1, 
            2 => 2, 
            3 => 3, 
            4 => 4
          ]
        ], 
        'blog.grid_column_gap' => [
          'label' => 'Column Gap', 
          'description' => 'Set the size of the gap between the grid columns.', 
          'type' => 'select', 
          'default' => '', 
          'options' => [
            'Small' => 'small', 
            'Medium' => 'medium', 
            'Default' => '', 
            'Large' => 'large', 
            'None' => 'collapse'
          ], 
          'enable' => 'blog.column != \'1\''
        ], 
        'blog.grid_row_gap' => [
          'label' => 'Row Gap', 
          'description' => 'Set the size of the gap between the grid rows.', 
          'type' => 'select', 
          'default' => '', 
          'options' => [
            'Small' => 'small', 
            'Medium' => 'medium', 
            'Default' => '', 
            'Large' => 'large', 
            'None' => 'collapse'
          ]
        ], 
        'blog.grid_breakpoint' => [
          'label' => 'Breakpoint', 
          'description' => 'Set the breakpoint from which grid items will stack.', 
          'type' => 'select', 
          'options' => [
            'Small (Phone Landscape)' => 's', 
            'Medium (Tablet Landscape)' => 'm', 
            'Large (Desktop)' => 'l', 
            'X-Large (Large Screens)' => 'xl'
          ], 
          'enable' => 'blog.column != \'1\''
        ], 
        'blog.grid_masonry' => [
          'label' => 'Masonry', 
          'description' => 'The masonry effect creates a layout free of gaps even if grid items have different heights. ', 
          'type' => 'checkbox', 
          'text' => 'Enable masonry effect', 
          'enable' => 'blog.column != \'1\''
        ], 
        'blog.grid_parallax' => [
          'label' => 'Parallax', 
          'description' => 'The parallax effect moves single grid columns at different speeds while scrolling. Define the vertical parallax offset in pixels.', 
          'type' => 'range', 
          'attrs' => [
            'min' => 0, 
            'max' => 600, 
            'step' => 10
          ], 
          'enable' => 'blog.column != \'1\''
        ], 
        'blog.image_align' => [
          'label' => 'Image Alignment', 
          'description' => 'Align the image to the top or place it between the title and the content.', 
          'type' => 'select', 
          'options' => [
            'Top' => 'top', 
            'Between' => 'between'
          ]
        ], 
        'blog.image_margin' => [
          'label' => 'Image Margin', 
          'description' => 'Set the top margin if the image is aligned between the title and the content.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ], 
          'enable' => 'blog.image_align == \'between\''
        ], 
        'blog.image_dimension' => [
          'type' => 'grid', 
          'description' => 'Setting just one value preserves the original proportions. The image will be resized and cropped automatically and where possible, high resolution images will be auto-generated.', 
          'fields' => [
            'blog.image_width' => [
              'label' => 'Image Width', 
              'width' => '1-2', 
              'attrs' => [
                'placeholder' => 'auto', 
                'lazy' => true
              ]
            ], 
            'blog.image_height' => [
              'label' => 'Image Height', 
              'width' => '1-2', 
              'attrs' => [
                'placeholder' => 'auto', 
                'lazy' => true
              ]
            ]
          ]
        ], 
        'blog.header_align' => [
          'label' => 'Alignment', 
          'description' => 'Align the title and meta text as well as the continue reading button.', 
          'type' => 'checkbox', 
          'text' => 'Center the title, meta text and button'
        ], 
        'blog.title_style' => [
          'label' => 'Title Style', 
          'description' => 'Title styles differ in font-size but may also come with a predefined color, size and font.', 
          'type' => 'select', 
          'options' => [
            'None' => '', 
            'H1' => 'h1', 
            'H2' => 'h2', 
            'H3' => 'h3', 
            'H4' => 'h4'
          ]
        ], 
        'blog.title_margin' => [
          'label' => 'Title Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'blog.meta_margin' => [
          'label' => 'Meta Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'blog.content_excerpt' => [
          'label' => 'Content', 
          'description' => 'Show the excerpt in the blog overview instead of the post text.', 
          'type' => 'checkbox', 
          'text' => 'Use excerpt'
        ], 
        'blog.content_length' => [
          'label' => 'Content Length', 
          'description' => 'Limit the content length to a number of characters. All HTML elements will be stripped.', 
          'type' => 'number'
        ], 
        'blog.content_margin' => [
          'label' => 'Content Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'blog.content_align' => [
          'label' => 'Content Alignment', 
          'type' => 'checkbox', 
          'text' => 'Center the content'
        ], 
        'blog.button_style' => [
          'label' => 'Button', 
          'description' => 'Select a style for the continue reading button.', 
          'type' => 'select', 
          'options' => [
            'Default' => 'default', 
            'Primary' => 'primary', 
            'Secondary' => 'secondary', 
            'Danger' => 'danger', 
            'Text' => 'text'
          ]
        ], 
        'blog.button_margin' => [
          'label' => 'Button Margin', 
          'description' => 'Set the top margin.', 
          'type' => 'select', 
          'options' => [
            'Small' => 'small', 
            'Default' => 'default', 
            'Medium' => 'medium', 
            'Large' => 'large', 
            'X-Large' => 'xlarge', 
            'None' => 'remove'
          ]
        ], 
        'blog.navigation' => [
          'label' => 'Navigation', 
          'description' => 'Use a numeric pagination or previous/next links to move between blog pages.', 
          'type' => 'select', 
          'options' => [
            'Pagination' => 'pagination', 
            'Previous/Next' => 'previous/next'
          ]
        ], 
        'blog.date' => [
          'label' => 'Display', 
          'type' => 'checkbox', 
          'text' => 'Show date'
        ], 
        'blog.author' => [
          'type' => 'checkbox', 
          'text' => 'Show author'
        ], 
        'blog.categories' => [
          'type' => 'checkbox', 
          'text' => 'Show categories'
        ], 
        'blog.comments' => [
          'type' => 'checkbox', 
          'text' => 'Show comments count'
        ], 
        'blog.content' => [
          'type' => 'checkbox', 
          'text' => 'Show content'
        ], 
        'blog.tags' => [
          'type' => 'checkbox', 
          'text' => 'Show tags'
        ], 
        'blog.button' => [
          'type' => 'checkbox', 
          'text' => 'Show button'
        ], 
        'blog.category_title' => [
          'type' => 'checkbox', 
          'text' => 'Show archive category title', 
          'description' => 'Show system fields for the blog. This option does not apply to single posts.'
        ]
      ], 
      'help' => [
        'Blog' => [[
            'title' => 'Layout', 
            'src' => 'site/docs/yootheme-pro/blog/wordpress/videos/layout.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/blog#layout', 
            'support' => 'support/search?tags=125&q=blog'
          ], [
            'title' => 'Image', 
            'src' => 'site/docs/yootheme-pro/blog/wordpress/videos/image.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/blog#image', 
            'support' => 'support/search?tags=125&q=blog'
          ], [
            'title' => 'Content', 
            'src' => 'site/docs/yootheme-pro/blog/wordpress/videos/content.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/blog#content', 
            'support' => 'support/search?tags=125&q=blog'
          ], [
            'title' => 'Navigation', 
            'src' => 'site/docs/yootheme-pro/blog/wordpress/videos/navigation.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/blog#navigation', 
            'support' => 'support/search?tags=125&q=navigation'
          ], [
            'title' => 'Excerpt', 
            'src' => 'site/docs/yootheme-pro/blog/wordpress/videos/excerpt.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/blog#excerpt', 
            'support' => 'support/search?tags=125&q=excerpt'
          ]], 
        'Image Field' => [[
            'title' => 'Images', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/images.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#images', 
            'support' => 'support/search?tags=125&q=image%20field'
          ], [
            'title' => 'Media Manager', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/media-manager.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#media-manager', 
            'support' => 'support/search?tags=125&q=media%20manager'
          ], [
            'title' => 'Unsplash Library', 
            'src' => 'site/docs/yootheme-pro/files-and-images/wordpress/videos/unsplash.mp4', 
            'documentation' => 'support/yootheme-pro/wordpress/files-and-images#unsplash-library', 
            'support' => 'support/search?tags=125&q=unsplash'
          ]]
      ]
    ]
  ]
];
