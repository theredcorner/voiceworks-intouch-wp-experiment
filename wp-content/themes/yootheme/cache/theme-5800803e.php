<?php // @file C:/wamp64/www/theredcorner/voiceworks-intouch/wp-content/themes/yootheme/vendor/yootheme/theme-wordpress/config/theme.json

return [
  'update' => 'https://yootheme.com/api/update/yootheme_wp', 
  'help_layout' => [
    'Customizer' => [[
        'title' => 'Save, Cancel and Close', 
        'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/save-cancel-close.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/customizer#save,-cancel-and-close', 
        'support' => 'support/search?tags=125&q=customizer%20save'
      ], [
        'title' => 'Contextual Help', 
        'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/contextual-help.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/customizer#contextual-help', 
        'support' => 'support/search?tags=125&q=contextual%20help'
      ], [
        'title' => 'Device Preview Buttons', 
        'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/device-preview-buttons.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/customizer#device-preview-buttons', 
        'support' => 'support/search?tags=125&q=customizer%20device%20preview'
      ], [
        'title' => 'Hide Sidebar', 
        'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/hide-sidebar.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/customizer#hide-sidebar', 
        'support' => 'support/search?tags=125&q=customizer%20hide%%20sidebar'
      ]], 
    'Widgets and Areas' => [[
        'title' => 'Widgets', 
        'src' => 'site/docs/yootheme-pro/widgets-and-modules/wordpress/videos/widgets.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widgets-and-modules', 
        'support' => 'support/search?tags=125&q=widgets'
      ], [
        'title' => 'Creating a Widget', 
        'src' => 'site/docs/yootheme-pro/widgets-and-modules/wordpress/videos/create-widget.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widgets-and-modules#widget-customizer', 
        'support' => 'support/search?tags=125&q=widgets'
      ], [
        'title' => 'Areas', 
        'src' => 'site/docs/yootheme-pro/widgets-and-modules/wordpress/videos/areas.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widgets-and-modules#widget-areas', 
        'support' => 'support/search?tags=125&q=module%20areas'
      ], [
        'title' => 'Widget Visibility', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-visibility.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#widget-visibility', 
        'support' => 'support/search?tags=125&q=widget%20visibility'
      ]], 
    'Widget Theme Options' => [[
        'title' => 'Default', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-theme-options-default.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#default-options', 
        'support' => 'support/search?tags=125&q=widget%20theme%20settings'
      ], [
        'title' => 'Appearance', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-theme-options-appearance.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#appearance-options', 
        'support' => 'support/search?tags=125&q=widget%20theme%20settings'
      ], [
        'title' => 'Grid', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-theme-options-grid.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#grid-options', 
        'support' => 'support/search?tags=125&q=widget%20theme%20settings%20grid'
      ], [
        'title' => 'List', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-theme-options-list.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#list-options', 
        'support' => 'support/search?tags=125&q=widget%20theme%20settings%20list'
      ], [
        'title' => 'Menu', 
        'src' => 'site/docs/yootheme-pro/widget-and-module-settings/wordpress/videos/widget-theme-options-menu.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/widget-and-module-settings#menu-options', 
        'support' => 'support/search?tags=125&q=widget%20theme%20settings%20menu'
      ]], 
    'Builder Widget' => [[
        'title' => 'Open the Builder Widget', 
        'src' => 'site/docs/yootheme-pro/builder-widget-and-module/wordpress/videos/open-builder-widget.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/builder-widget-and-module', 
        'support' => 'support/search?tags=125&q=builder%20widget'
      ], [
        'title' => 'Advanced Layouts', 
        'src' => 'site/docs/yootheme-pro/builder-widget-and-module/wordpress/videos/advanced-layouts.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/builder-widget-and-module#advanced-layouts', 
        'support' => 'support/search?tags=125&q=builder%20widget'
      ]], 
    'Menus' => [[
        'title' => 'Menus', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/menus.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus', 
        'support' => 'support/search?tags=125&q=menu'
      ], [
        'title' => 'Locations', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/menu-locations.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#menu-locations', 
        'support' => 'support/search?tags=125&q=menu%20location'
      ], [
        'title' => 'Widget', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/menu-widget.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#menu-widget', 
        'support' => 'support/search?tags=125&q=menu%20widget'
      ]], 
    'Menu Items' => [[
        'title' => 'Divider', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/menu-divider.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#menu-divider', 
        'support' => 'support/search?tags=125&q=menu%20divider'
      ], [
        'title' => 'Heading', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/menu-heading.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#menu-heading', 
        'support' => 'support/search?tags=125&q=menu%20heading'
      ], [
        'title' => 'Navbar Text Item', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/navbar-text-item.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#navbar-text-item', 
        'support' => 'support/search?tags=125&q=navbar%20text%20item'
      ], [
        'title' => 'Accordion Menu', 
        'src' => 'site/docs/yootheme-pro/menus/wordpress/videos/accordion-menu.mp4', 
        'documentation' => 'support/yootheme-pro/wordpress/menus#accordion-menu', 
        'support' => 'support/search?tags=125&q=accordion%20menu'
      ]]
  ], 
  'help_settings' => [[
      'title' => 'Save, Cancel and Close', 
      'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/save-cancel-close.mp4', 
      'documentation' => 'support/yootheme-pro/wordpress/customizer#save,-cancel-and-close', 
      'support' => 'support/search?tags=125&q=customizer%20save'
    ], [
      'title' => 'Contextual Help', 
      'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/contextual-help.mp4', 
      'documentation' => 'support/yootheme-pro/wordpress/customizer#contextual-help', 
      'support' => 'support/search?tags=125&q=contextual%20help'
    ], [
      'title' => 'Device Preview Buttons', 
      'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/device-preview-buttons.mp4', 
      'documentation' => 'support/yootheme-pro/wordpress/customizer#device-preview-buttons', 
      'support' => 'support/search?tags=125&q=customizer%20device%20preview'
    ], [
      'title' => 'Hide Sidebar', 
      'src' => 'site/docs/yootheme-pro/customizer/wordpress/videos/hide-sidebar.mp4', 
      'documentation' => 'support/yootheme-pro/wordpress/customizer#hide-sidebar', 
      'support' => 'support/search?tags=125&q=customizer%20hide%%20sidebar'
    ]]
];
