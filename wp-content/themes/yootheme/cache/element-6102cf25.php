<?php // @file C:/wamp64/www/theredcorner/voiceworks-intouch/wp-content/themes/yootheme/vendor/yootheme/builder/elements/layout/element.json

return [
  'name' => 'layout', 
  'title' => 'Layout', 
  'container' => true, 
  'templates' => [
    'render' => $this->applyFilter('./templates/template.php', ['path']), 
    'content' => $this->applyFilter('./templates/template.php', ['path'])
  ]
];
